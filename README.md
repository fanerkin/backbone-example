# backbone + require.js + handlebars example

### "When in doubt, use brute force".
* One view for the collection.
* Neither backbone.collectionView/jQueryUI, nor individual bindings.
* Uses HTML5 drag-n-drop, stores indexes in data-id attributes.

### Why?
* Not to optimize prematurely: if we have > 100 TODOs/people in a queue, speed is not the issue.
* A virtual DOM library can be utilized to gain speed burst while keeping things simple.

### usage
```sh
$ cd /path/to/index.html
$ python -m SimpleHTTPServer 8000
```

then http://localhost:8000 in browser
