define(function (require) {
	return function $ajax(responseText, delay) {
		return function (o) {
			var data;

			if (o.dataType == 'json') {
				try {
					data = JSON.parse(responseText)
				} catch (ex) {
					console.log(ex);
					return;
				}
			} else {
				data = responseText;
			}

			setTimeout(function () {
				o.success(data);
			}, delay || 100);
		};
	};
});
