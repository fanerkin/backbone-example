define(function (require) {
	return function getExampleData() {
		var
			// must not validate if empty name
			names = {
				male: [ 'petr', 'ivan', 'sidor', 'yefim', 'konstantin' ],
				female: [ 'elvira', 'olga', 'marina',
							'viktoria', 'katerina' ]
			},
			endings = {
				male: 'ov',
				female: 'ova'
			},
			name,
			surname,
			age,
			gender,
			getRandom = require('util/getRandom'),
			data = [],
			i;

		// must not validate if i < 1
		for ( i = 1; i < 15; i++ ) {
			gender = getRandom(0, 2) ? 'male' : 'female';
			name = names[gender][getRandom(0, names[gender].length)];
			surname = names.male[getRandom(0, names.male.length)] +
				endings[gender];
			age = getRandom(20, 50);

			data.push({
				index: i,
				name: name,
				surname: surname,
				age: age,
				gender: gender
			});
		}

		return JSON.stringify(_.shuffle(data));
	};
});
