define(function (require) {
	return function parseJSON(json) {
		var data;

		try {
			data = JSON.parse(json);
		} catch (ex) {
			console.log(ex);
			data = {};
		}

		return data;
	};
});
