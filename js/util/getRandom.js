define(function (require) {
	// including from, excluding to, like [].slice
	return function getRandom(from, to) {
		var spread = to - from;
		return Math.floor(Math.random() * spread) + from;
	};
});
