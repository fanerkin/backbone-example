define(function (require) {
	require('backbone');

	return Backbone.View.extend({
		template: require('views/Table.template'),
		initialize: function (o) {
			this.options = o || {};

			// put any listeners you want here
			this.listenTo(this.collection, 'change', this.render);
			if (o.shouldUpdateOnSwap) {
				this.listenTo(
					this.collection,
					'swapped',
					this.render
				);
				this.events = require('views/Table.events')(this.collection);
			}
		},
		// logic not to be put into template
		present: function (o) {
			// not this.collection.where(o.where)
			// because if o.where is undefined, should return all
			return _.chain( this.collection.toJSON() )
				.where(o.where)
				.filter(o.filter || function () { return true; })
				.sortBy(function (person) {
					return person[o.sortBy];
				})
				.value();
		},
		render: function () {
			var o = this.options,
				firstChild = this.el.firstChild,
				tmp;

			// using temp element for less reflows
			// also, rerendering whole view means
			// always correct representation of data
			tmp = $('<div>').html(
				this.template({
					persons: this.present(o),
					draggable: o.draggable,
					className: o.className,
					caption: o.caption
				})
			)[0];

			if ( !firstChild ) {
				this.$el.append(tmp.firstChild);
			} else {
				$(firstChild).replaceWith(tmp.firstChild);
			}
			console.log(o.caption + ' table rendered');

			return this;
		}
	});
});
