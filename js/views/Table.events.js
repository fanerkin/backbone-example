define(function (require) {
	return function getListeners(persons) {
		var sourceId;

		function getIndex(el) {
			return +$(el).attr('data-id');
		}

		function onDragStart(e) {
			$(e.target).addClass('gray');
			sourceId = getIndex(e.target);
			e.originalEvent.dataTransfer.effectAllowed = 'move';
			// FF needs this to allow d'n'd
			e.originalEvent.dataTransfer.setData('text/html', '');
		}

		function onDragOver(e) {
			// Necessary. Allows us to drop.
			e.preventDefault();
			e.stopPropagation();

			e.originalEvent.dataTransfer.dropEffect = 'move';
		}

		function onDragEnter(e) {
			var $target = $(e.target).closest('[draggable]');
			$target.addClass('over');
		}

		function onDragLeave(e) {
			var $target = $(e.target).closest('[draggable]');
			$target.removeClass('over');
		}

		function onDrop(e, cb) {
			var targetId = getIndex(
					$(e.target).closest('[draggable]')[0]
				);

			e.preventDefault();
			e.stopPropagation();

			if (
				// the same table
				sourceId &&
				// dropping on a row
				targetId &&
				// which is different row
				sourceId != targetId
			) {
				persons.trigger('swap', sourceId, targetId);
			}
		}

		function onDragEnd(e) {
			$('.over').removeClass('over');
		}

		return {
			dragstart: onDragStart,
			dragenter: onDragEnter,
			dragover: onDragOver,
			dragleave: onDragLeave,
			drop: onDrop,
			dragend: onDragEnd
		};
	};
});
