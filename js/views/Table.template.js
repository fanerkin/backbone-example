define(function (require) {
	var Handlebars = require('handlebars'),
		table = require('text!../../templates/table.hbs'),
		person = require('text!../../templates/person.hbs');

	Handlebars.registerPartial('person', person);

	return Handlebars.compile(table);
});
