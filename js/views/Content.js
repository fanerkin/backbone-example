define(function (require) {
	require('backbone');

	return Backbone.View.extend({
		className: 'content',
		attributes: { id: 'content' },
		render: function () {
			var
				Table = require('views/Table'),
				males,
				females;

			// it is possible to change behaviour later
			// through Table.options
			males = new Table({
				collection: this.collection,
				className: 'males',
				caption: 'men',
				where: { gender: 'male' },
				sortBy: 'index'
			});
			females = new Table({
				collection: this.collection,
				className: 'females',
				caption: 'women',
				where: { gender: 'female' },
				sortBy: 'index',
				shouldUpdateOnSwap: true,
				draggable: true
			});

			this.$el.append(males.render().el);
			this.$el.append(females.render().el);
			console.log('content rendered');

			return this;
		}
	});
});
