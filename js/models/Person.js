define(function (require) {
	require('backbone');

	return Backbone.Model.extend({
		idAttribute: 'index',
		initialize: function () {
			this.on('invalid', function (model, error) {
				console.log(error);
			});
		},
		validate: function (attributes) {
			var id = attributes[this.idAttribute],
				name = attributes.name,
				errors = [];

			if ( ( id|0 ) !== id || id < 1 ) {
				errors.push(this.idAttribute + ' must be a positive integer');
			}
			if ( typeof name != 'string' || !name ) {
				errors.push('name must be a non-empty string');
			}
			if (errors.length) return errors;
		}
	});
});
