define(function (require) {
	require('backbone');

	return Backbone.Collection.extend({
		model: require('models/Person'),
		initialize: function () {
			this.on('invalid', function (model, error) {
				console.log(error);
			});
			this.on('swap', this.swap);
		},
		/**
		 * @param  {Number} a
		 * @param  {Number} b
		 */
		swap: function (a, b) {
			var
				idAttribute = this.models[0].idAttribute,
				item1,
				item2,
				tmpElement;

			if (a == b) return;

			item1 = this._byId[a];
			item2 = this._byId[b];
			tmpElement = item1;

			item1.attributes[idAttribute] = b;
			item1.id = b;
			this._byId[a] = item2;

			item2.attributes[idAttribute] = a;
			item2.id = a;
			this._byId[b] = tmpElement;

			// the only event triggered
			this.trigger('swapped', a, b);
		}
	});
});
