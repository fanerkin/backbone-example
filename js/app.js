requirejs.config({
    paths: {
        underscore: './lib/underscore',
        jquery: './lib/jquery',
        backbone: './lib/backbone',
        handlebars: './lib/handlebars',
        text: './lib/text'
    }
});

define(function (require) {
	require('jquery');

	var
		// TODO this is for testing, put initial data in html when ready
		json = require('text!../data/data.json'),
		parseJSON = require('util/parseJSON'),
		data,
		$body = $('body'),
		Persons = require('collections/Persons'),
		persons,
		Content = require('views/Content'),
		content;

	data = parseJSON(json);

	persons = new Persons;
	persons.reset(data, { validate: true });

	content = new Content({
		collection: persons
	});
	$body.append(content.render().el);
});
